pub use printf_proc::print_format as printf;
pub use printf_proc::string_format as sprintf;

////////////////////////////////////////////////////////////////////////////////
//
// This doesn't seem to work with my procedural macros & hygiene...
//   the call-site idents don't resolve
//
////////////////////////////////////////////////////////////////////////////////
//
// #[macro_export]
// macro_rules! printf {
//     ($fmt:expr $(,$arg:expr)*) => {{
//         let s = sprintf!($fmt $(,$arg)*);
//         print!("{}", s);
//     }};
// }
//
////////////////////////////////////////////////////////////////////////////////

pub mod util {
    pub fn id<T>(x: T) -> T {
        x
    }
}
