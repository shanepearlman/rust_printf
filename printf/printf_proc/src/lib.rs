#![feature(slice_patterns)]

extern crate proc_macro;

use rand;
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use proc_macro2::Ident as Ident2;
use proc_macro2::Span as Span2;
use syn::{Token, Result};
use syn::parse::{Parse, ParseStream};
use quote::quote;

struct FormatArgs {
    fmt: syn::LitStr,
    args: Vec<syn::Expr>,
}

impl Parse for FormatArgs {
    fn parse(input: ParseStream) -> Result<Self> {
        let fmt = input.parse()?;

        let mut args = vec![];
        while ! input.is_empty() {
            let _: Token![,] = input.parse()?;
            args.push(input.parse()?);
        }

        Ok(FormatArgs {
            fmt,
            args,
        })
    }
}

#[proc_macro]
pub fn print_format(tokens: TokenStream) -> TokenStream {
    let string = string_format(tokens);
    let string = TokenStream2::from(string);

    // Make a random identifier, because Span2::def_site() is not available yet
    let id: u8 = rand::random();
    let result = Ident2::new(&format!("r_{}", id), Span2::call_site());
    let emit = quote! {{
        let #result = #string;
        print!("{}", #result);
    }};

    // println!("{}", emit.to_string());
    emit.into()
}

#[proc_macro]
pub fn string_format(tokens: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(tokens as FormatArgs);
    let format_string = input.fmt.value();

    // Not sure if this is necessary
    assert!(format_string.is_ascii());

    let mut format_spec = parse_format_string(format_string.as_bytes());
    optimize_spec(&mut format_spec);

    // Make a random identifier, because Span2::def_site() is not available yet
    let id: u8 = rand::random();
    let result = Ident2::new(&format!("s_{}", id), Span2::call_site());
    let statements = emit_format(format_spec, input.args, result.clone());
    let emit = quote! {{
        // $crate doesn't seem to work in procedural macros

        // This will create name conflicts
        let mut #result = "".to_string();
        #statements
        #result
    }};

    // println!("{}", emit.to_string());
    emit.into()
}

fn emit_format(spec: Spec, mut args: Vec<syn::Expr>, result: Ident2) -> TokenStream2 {
    let result = spec.iter().flat_map(|item| {
        match item {
            SpecItem::LitString(s) => {
                quote! {
                    #result = format!("{}{}", #result, #s);
                }
            }
            _otherwise => {
                if args.is_empty() {
                    panic!("too few arguments for format string");
                }
                emit_item(item.clone(), args.remove(0), result.clone())
            }
        }
    }).collect();

    if ! args.is_empty() {
        panic!("too many arguments for format string");
    }
    result
}

fn emit_item(spec_item: SpecItem, arg: syn::Expr, result: Ident2) -> TokenStream2 {
    let id = quote!(printf::util::id);

    match spec_item {
        SpecItem::Decimal => {
            quote! {
                #result = format!("{}{}", #result, #id::<i32>(#arg));
            }
        }
        SpecItem::Float => {
            quote! {
                #result = format!("{}{}", #result, #id::<f32>(#arg));
            }
        }
        SpecItem::String => {
            quote! {
                #result = format!("{}{}", #result, #id::<&str>(#arg));
            }
        }
        _otherwise => {
            unreachable!()
        }
    }
}

type Spec = Vec<SpecItem>;

#[derive(Debug, Clone)]
enum SpecItem {
    LitChar(u8),
    LitString(String),
    Decimal,
    Float,
    String,
}

fn cons<T>(x: T, mut xs: Vec<T>) -> Vec<T> {
    xs.insert(0, x);
    xs
}

fn parse_format_string(xs: &[u8]) -> Spec {
    match xs {
        [                ] => vec![],
        [b'%', b'%', xs..] => cons(SpecItem::LitChar(b'%'), parse_format_string(xs)),
        [b'%',       xs..] => parse_format_spec(xs),
        [   x,       xs..] => cons(SpecItem::LitChar(*x), parse_format_string(xs)),
    }
}

fn parse_format_spec(xs: &[u8]) -> Spec {
    match xs {
        [          ] => panic!("expected format specifier following '%' character"),
        [b'd', xs..] => cons(SpecItem::Decimal, parse_format_string(xs)),
        [b'f', xs..] => cons(SpecItem::Float, parse_format_string(xs)),
        [b's', xs..] => cons(SpecItem::String, parse_format_string(xs)),
        [   x,   ..] => panic!("illegal format specifier '{}'", *x as char),
    }
}

fn optimize_spec(spec: &mut Spec) {
    let mut i = 0;
    let mut last_is_string = false;

    while i < spec.len() {
        match &spec[i] {
            SpecItem::LitChar(c) => {
                let s = String::from_utf8_lossy(&[*c]).to_string();

                if last_is_string {
                    match &mut spec[i - 1] {
                        SpecItem::LitString(s2) => s2.push_str(&s),
                        _otherwise              => unreachable!(),
                    }
                    spec.remove(i);
                    i -= 1;  // Negate the loop increment because we removed an item
                } else {
                    spec[i] = SpecItem::LitString(s);
                }
                last_is_string = true;
            },
            _otherwise => {
                last_is_string = false;
            },
        }
        i += 1;
    }
}
