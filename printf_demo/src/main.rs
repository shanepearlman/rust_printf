#![feature(trace_macros)]
#![feature(proc_macro_hygiene)]

use printf::*;

fn main() {
    trace_macros!(true);

    let name = "world";
    printf!("%s %s!\n", "Hello", name);

    printf!("... hello, computer...\n");

    let a = 10;
    let b = 12.5;

    printf!("%d ", a);
    printf!("%f\n", b);

    let s = sprintf!("a = %d; b = %f; a + b = %f\n", a, b, (a as f32) + b);
    print!("{}", s);

    printf!("%d%%\n", a);
}
